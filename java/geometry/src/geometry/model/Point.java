package geometry.model;

public class Point {
	private double x;
	private double y;

	public double getX() { return this.x; }
	public double getY() { return this.y; }
	
	public void setX(double x) { this.x = x; }
	public void setY(double y) { this.y = y; }
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (obj == this) return true;
		
		if (!(obj instanceof Point)) return false;
		
		Point compare = (Point)obj;
		
		return compare.getX() == this.getX() && compare.getY() == this.getY();
	}
}

