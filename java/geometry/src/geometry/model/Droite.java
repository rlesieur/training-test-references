package geometry.model;

public class Droite {
	private Point a;
	private Point b;
	
	private double p;
	private double d;

	public Droite(Point a, Point b)
	{
		this.a = a;
		this.b = b;
		
		/*
		 * y = px + d
		 * 
		 * p = (y2 - y1) / (x2 - x1)
		 * d = (x2y1) - (x1y2) / (x2 - x1) 
		 * */
		this.p = (b.getY() - a.getY()) / (b.getX() - a.getX());
		this.d = ( (b.getX() * a.getY()) - (a.getX() * b.getY()) ) / (b.getX() - a.getX() );
	}

	public double getP() {
		return this.p;
	}

	public double getD() {
		return this.d;
	}
	
	
}
