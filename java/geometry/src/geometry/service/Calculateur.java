package geometry.service;

import geometry.model.Droite;
import geometry.model.Point;

public class Calculateur {

	public double distance(Point a, Point b) {
		double x = Math.pow(b.getX() - a.getX(), 2);
		double y = Math.pow(b.getY() - a.getY(), 2);
		
		return Math.sqrt(x + y);
	}

	public Point getIntersection(Droite d1, Droite d2) {
		Point output = new Point();
		
		double d = d1.getD() - d2.getD();
		double p = d2.getP() - d1.getP();
		
		double x = d / p;
		double y = (d1.getP() * x) + d1.getD();
		
		output.setX(x);
		output.setY(y);
		
		return output;
	}

	public Boolean areParalels(Droite d1, Droite d2) {
		Point t = getIntersection(d1, d2);
		
		return 
				(t.getX() == Double.NEGATIVE_INFINITY || t.getX() == Double.POSITIVE_INFINITY) &&
				(t.getY() == Double.NEGATIVE_INFINITY || t.getY() == Double.POSITIVE_INFINITY);
	}

}
