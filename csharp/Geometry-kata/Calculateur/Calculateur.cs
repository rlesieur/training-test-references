﻿using Geometry.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Service
{
    public class Calculateur
    {
        public double Distance(Point a, Point b)
        {
            double x = Math.Pow((double)(b.X - a.X), 2);
            double y = Math.Pow((double)(b.Y - a.Y), 2);

            return Math.Sqrt(x + y);
        }

        public Point Intersection(Droite d1, Droite d2)
        {
            decimal d = d1.D - d2.D;
            decimal p = d2.P - d1.P;

            if (p == 0) return null;

            decimal x = d / p;
            decimal y = (d1.P * x) + d1.D;

            return new Point(x, y);
        }

        public bool Parallels(Droite d1, Droite d2)
        {
            var intersec = Intersection(d1, d2);

            return intersec == null;
        }
    }
}
