﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Model
{
    public class Droite
    {
        public Point A { get; }
        public Point B { get; }

        public decimal P { get; private set; }
        public decimal D { get; private set; }

        public Droite(Point A, Point B)
        {
            this.A = A;
            this.B = B;

            CalculFonctionDroite();
        }

        private void CalculFonctionDroite()
        {
            this.P = (B.Y - A.Y) / (B.X - A.X);
            this.D = ((B.X * A.Y) - (A.X * B.Y)) / (B.X - A.X);
        }
    }
}
