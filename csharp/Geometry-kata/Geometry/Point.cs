﻿namespace Geometry.Model
{
    public class Point
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }

        public override bool Equals(object obj)
        {
            var p2 = obj as Point;
            if (p2 == null) return base.Equals(obj);

            return (p2.X == X && p2.Y == Y);
        }

        public Point(decimal x, decimal y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
